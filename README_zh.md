# Luckysheet-Import-TeamEdit-Export
***
> - 利用[Lucksheet](https://gitee.com/mengshukeji/Luckysheet)实现的电子表格文件（.xlsx）在线导入，团队协作编辑，结果文件导出三个功能。

> - [视频演示地址](https://www.bilibili.com/video/BV1Gf4y1h7r3/)：https://www.bilibili.com/video/BV1Gf4y1h7r3/

***

## 主要功能
1. 表格导入展示-http://localhost:8081/import
2. 团队在线协同编辑表格-http://localhost:8081/edit
3. 结果文件导出-http://localhost:8081/export

## 参考项目以及致谢
- [Lucksheet](https://gitee.com/mengshukeji/Luckysheet) Lucksheet Web电子表格
- [SpringBoot](https://spring.io/projects/spring-boot) 后端数据处理
- [leverDB](https://github.com/google/leveldb.git) 数据存储
- [ecsheet](https://github.com/DilemmaVi/ecsheet) 基于MongoDB存储
- [luckysheet保存恢复](https://gitee.com/ichiva/luckysheet-saved-in-recovery) luckysheet保存恢复

## tips of pull request 

欢迎大家一起来 pull request 

## 除责申明
1. 本开源项目仅为技术交流此一目的，严禁用于其他任何商业、违法犯罪、恶意攻击等行为；
3. 若第三者用此项目侵犯相关网站权益，一切责任自负；
2. 若本项目侵犯相关网站、个人，组织机构权益，请及时联系作者；

## 联系作者

- [oschina](http://git.oschina.net/liinux)
- [cnblogs](http://www.cnblogs.com/liinux)
- [github](https://github.com/liinnux)

## 扫码关注
<table>
    <tr>
		<td>作者微信</td>
        <td><img src="http://tigerxue.gitee.io/liinux-images/docs/img/alukesi.jpg" /></td>
    </tr>
</table>