package com.tompai.sheet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
* @desc: Luckysheet-online-teamwork-edit-demo
* @name: OnlineSheetEditApplication.java
* @author: tompai
* @email：liinux@qq.com
* @createTime: 2021年5月22日 下午5:18:59
* @history:
* @version: v1.0
*/
@SpringBootApplication
public class OnlineSheetEditApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineSheetEditApplication.class, args);
	}
}
