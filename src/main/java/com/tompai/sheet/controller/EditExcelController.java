package com.tompai.sheet.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.iq80.leveldb.DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tompai.sheet.config.KeysConfig;

import lombok.extern.slf4j.Slf4j;

/**
* @desc: Luckysheet-online-teamwork-edit-demo
* @name: EditExcelController.java
* @author: tompai
* @email：liinux@qq.com
* @createTime: 2021年5月22日 下午5:18:59
* @history:
* @version: v1.0
*/
@Slf4j
@CrossOrigin
@RestController
public class EditExcelController {

	@Autowired
	DB db;

	@GetMapping("/version")
	public Object version() {
		return new Object() {
			public String version = "v0.0.3";
		};
	}

	// 取文件
	@RequestMapping("/get")
	public Object get() throws IOException {
		return new String(db.get(KeysConfig.FILE), StandardCharsets.UTF_8);
	}

	// 设置文件
	@PostMapping("/set")
	public Object set(String jsonExcel) throws IOException {
		db.put(KeysConfig.FILE, jsonExcel.getBytes(StandardCharsets.UTF_8));
		return true;
	}
}
