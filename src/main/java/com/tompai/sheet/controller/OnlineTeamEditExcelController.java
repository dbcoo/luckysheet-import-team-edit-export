
/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.sheet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
* @desc: Luckysheet-online-teamwork-edit-demo
* @name: OnlineTeamEditExcelController.java
* @author: tompai
* @email：liinux@qq.com
* @createTime: 2021年5月22日 下午5:52:52
* @history:
* @version: v1.0
*/
@Controller
public class OnlineTeamEditExcelController {

	@GetMapping("/edit")
	public String editExcel() {
		return "edit";
	}
}
