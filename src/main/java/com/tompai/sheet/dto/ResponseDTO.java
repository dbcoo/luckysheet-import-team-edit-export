package com.tompai.sheet.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
* @desc: Luckysheet-online-teamwork-edit-demo
* @name: ResponseDTO.java
* @author: tompai
* @email：liinux@qq.com
* @createTime: 2021年5月22日 下午5:18:59
* @history:
* @version: v1.0
*/
@Data
@AllArgsConstructor
public class ResponseDTO implements Serializable {

	private static final long serialVersionUID = -275582248840137389L;

	private Integer type;

	private String id;

	private String username;

	private String data;

	public static ResponseDTO success(String id, String username, String data) {
		return new ResponseDTO(1, id, username, data);
	}

	public static ResponseDTO update(String id, String username, String data) {
		return new ResponseDTO(2, id, username, data);
	}

	public static ResponseDTO mv(String id, String username, String data) {
		return new ResponseDTO(3, id, username, data);
	}

	public static ResponseDTO bulkUpdate(String id, String username, String data) {
		return new ResponseDTO(4, id, username, data);
	}

}
