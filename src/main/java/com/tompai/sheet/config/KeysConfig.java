package com.tompai.sheet.config;

import java.nio.charset.StandardCharsets;
/**
* @desc: Luckysheet-online-teamwork-edit-demo
* @name: KeysConfig.java
* @author: tompai
* @email：liinux@qq.com
* @createTime: 2021年5月22日 下午5:18:59
* @history:
* @version: v1.0
*/
public interface KeysConfig {
	// 共享文件
	byte[] FILE = "sys_f2".getBytes(StandardCharsets.UTF_8);
}
